## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)

## General info
Two Seprated JAVA apps, which are Ducks and Turkeys.
Ducks implements strategy pattern, Turkeys implements adapter pattern.

## Technologies
Project is created with:
* Java
* Maven