package bonusAssignment.Turkeys;

public class Turkey {
    public Turkey(){}

    public void display(){
        System.out.println("This is a turkey.");
    }
    
    public void gobble(){
        System.out.println("Gobble!");
    }

    public void fly(){
        System.out.println("I fly a short distance!");
    }
}