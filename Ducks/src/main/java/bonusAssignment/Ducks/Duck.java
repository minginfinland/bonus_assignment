package bonusAssignment.Ducks;

public abstract class Duck {
    FlyBehavior flyBehavior;

    public void quack(){
        System.out.println("QUACK!");
    }

    public void display(){
        System.out.println("This is a generic duck.");
    }

    public void performFly(){
        flyBehavior.fly();
    }

    public void setFlyMode(FlyBehavior fly){
        flyBehavior = fly;
    }
}
