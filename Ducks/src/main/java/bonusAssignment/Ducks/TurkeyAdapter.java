package bonusAssignment.Ducks;

import bonusAssignment.Turkeys.Turkey;


public class TurkeyAdapter extends Duck {
    Turkey turkey;
    public TurkeyAdapter(Turkey turkey){
        this.turkey = turkey;
    }

    @Override
    public void display(){
        turkey.display();
    }

    public void quack(){
        turkey.gobble();
    }

    public void performFly(){
        for(int i=0; i<3; i++){
            turkey.fly();
        }
    }
}
