package bonusAssignment.Ducks;

public class MallardDuck extends Duck  {

    public MallardDuck(){
        flyBehavior = new FlyWithWings();
    }

    @Override
    public void display(){
        System.out.println("This is a mallard duck.");
    }
}
