package bonusAssignment.Ducks;
import bonusAssignment.Turkeys.Turkey;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Duck redheadDuck = new RedHeadDuck();
        printout(1, redheadDuck);

        Duck mallardDuck = new MallardDuck();
        printout(2, mallardDuck);

        Duck rubberDuck = new RubberDuck();
        printout(3, rubberDuck);

        Turkey turkey = new Turkey();
        Duck turkeyDuck = new TurkeyAdapter(turkey);
        printout(4, turkeyDuck);
    }

    private static void printout(int ndx, Duck duck){
        System.out.println( "....." + ndx + " duck created" );
        duck.display();
        duck.quack();
        duck.performFly();
    }
}
