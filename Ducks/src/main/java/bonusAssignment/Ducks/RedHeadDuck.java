package bonusAssignment.Ducks;

public class RedHeadDuck extends Duck {
    public RedHeadDuck(){
        flyBehavior = new FlyWithWings();
    }

    @Override
    public void display(){
        System.out.println("This is a redhead duck.");
    }
}
